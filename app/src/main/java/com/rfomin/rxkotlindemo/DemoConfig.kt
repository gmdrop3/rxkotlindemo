package com.rfomin.rxkotlindemo

object DemoConfig {
    const val DELETE_MOVIES_FROM_DATABASE_ON_APP_START = true
    const val LOAD_MOVIES_FROM_THE_INTERNET = true
}