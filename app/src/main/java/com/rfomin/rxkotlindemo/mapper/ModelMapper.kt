package com.rfomin.rxkotlindemo.mapper

import com.rfomin.rxkotlindemo.database.model.MovieModel
import com.rfomin.rxkotlindemo.model.TmdbMovieResponse

fun TmdbMovieResponse.toMovieModelList(): List<MovieModel> {
    return results.map { tmdbMovie ->
        MovieModel(
            id = tmdbMovie.id,
            vote_average = tmdbMovie.vote_average,
            title = tmdbMovie.title,
            overview = tmdbMovie.overview,
            adult = tmdbMovie.adult,
            poster_path = tmdbMovie.poster_path
        )
    }
}