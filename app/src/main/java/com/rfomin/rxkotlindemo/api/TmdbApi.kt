package com.rfomin.rxkotlindemo.api

import com.rfomin.rxkotlindemo.model.TmdbMovieResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface TmdbApi {
    @GET("movie/popular")
    fun getPopularMovies() : Single<TmdbMovieResponse>

    @GET("search/movie")
    fun getSearchMovie(@Query("query") searchString: String) : Single<TmdbMovieResponse>
}
