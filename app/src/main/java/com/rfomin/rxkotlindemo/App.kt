package com.rfomin.rxkotlindemo

import android.app.Application
import android.content.Context
import com.rfomin.rxkotlindemo.dagger.AppComponent
import com.rfomin.rxkotlindemo.dagger.DaggerAppComponent
import dagger.Module
import dagger.Provides

@Module
class App: Application() {
    companion object {
        private lateinit var instance: App
        fun getContext(): App = instance

        private lateinit var daggerAppComponent: AppComponent
        fun getDaggerAppComponent() = daggerAppComponent
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        daggerAppComponent = DaggerAppComponent.builder().build()
        if (DemoConfig.DELETE_MOVIES_FROM_DATABASE_ON_APP_START) {
            Thread(Runnable {
                //For demo purposes we delete all the data from database on each application run
                daggerAppComponent.provideDataBase().getMovieDao().deleteAll()
            }).start()
        }
    }

    @Provides
    fun provideContext(): Context = instance
}