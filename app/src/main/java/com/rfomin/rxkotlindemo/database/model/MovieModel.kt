package com.rfomin.rxkotlindemo.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

interface Movie

@Entity
data class MovieModel(
    @PrimaryKey
    val id: Int,
    val vote_average: Double,
    val title: String,
    val overview: String,
    val adult: Boolean,
    val poster_path: String?
): Movie

object EmptyMovie: Movie {
    fun model() = this
}