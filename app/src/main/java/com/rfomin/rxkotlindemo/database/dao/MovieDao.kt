package com.rfomin.rxkotlindemo.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.IGNORE
import com.rfomin.rxkotlindemo.database.model.MovieModel
import io.reactivex.Observable

@Dao
interface MovieDao {
    @Query("SELECT * FROM MovieModel")
    fun getAllMoviesRx(): Observable<List<MovieModel>>

    @Query("SELECT * FROM MovieModel")
    fun getAllMoviesLiveData(): LiveData<List<MovieModel>>

    @Query("SELECT * FROM MovieModel WHERE title LIKE :searchString ORDER BY title")
    fun searchMoviesRx(searchString: String): Observable<List<MovieModel>>

    @Insert(onConflict = IGNORE)
    fun insert(movieList: List<MovieModel>)

    @Delete
    fun delete(movieList: List<MovieModel>)

    @Query("DELETE FROM MovieModel")
    fun deleteAll()

    @Update
    fun update(movieList: List<MovieModel>)
}