package com.rfomin.rxkotlindemo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rfomin.rxkotlindemo.database.dao.MovieDao
import com.rfomin.rxkotlindemo.database.model.MovieModel

@Database(entities = [MovieModel::class], version = 1, exportSchema = false)
abstract class MoviesDatabase : RoomDatabase() {
    abstract fun getMovieDao() : MovieDao
}