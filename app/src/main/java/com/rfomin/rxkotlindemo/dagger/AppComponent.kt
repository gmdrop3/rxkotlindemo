package com.rfomin.rxkotlindemo.dagger

import com.rfomin.rxkotlindemo.App
import com.rfomin.rxkotlindemo.api.ApiFactory
import com.rfomin.rxkotlindemo.api.TmdbApi
import com.rfomin.rxkotlindemo.dagger.provider.DaggerProvider
import com.rfomin.rxkotlindemo.database.MoviesDatabase
import com.rfomin.rxkotlindemo.dataprovider.DataProvider
import dagger.Component
import javax.inject.Singleton

@Component(modules = [ApiFactory::class
    , DaggerProvider::class
    , App::class])
@Singleton
interface AppComponent {
    fun provideApi(): TmdbApi
    fun provideApiFactory(): ApiFactory
    fun provideDataProvider(): DataProvider

    fun provideDataBase(): MoviesDatabase
}