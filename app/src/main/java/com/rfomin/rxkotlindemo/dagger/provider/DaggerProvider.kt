package com.rfomin.rxkotlindemo.dagger.provider

import android.content.Context
import androidx.room.Room
import com.rfomin.rxkotlindemo.api.ApiFactory
import com.rfomin.rxkotlindemo.database.MoviesDatabase
import com.rfomin.rxkotlindemo.dataprovider.DataProvider
import com.rfomin.rxkotlindemo.dataprovider.MovieDataProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DaggerProvider{
    @Provides
    @Singleton
    @JvmStatic
    fun provideDataProvider(): DataProvider {
        return MovieDataProvider()
    }

    @Provides
    @Singleton
    @JvmStatic
    fun provideApiFactory(): ApiFactory {
        return ApiFactory()
    }

    @Provides
    @Singleton
    @JvmStatic
    fun provideDatabase(context: Context): MoviesDatabase {
        return Room.databaseBuilder(context, MoviesDatabase::class.java, "movies.db").build()
    }

}