package com.rfomin.rxkotlindemo.rx

import android.widget.Toast
import com.rfomin.rxkotlindemo.App
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/** Repeats and retries an observable, with delays between attempts. */
class RxRepeat<T> internal constructor(
    private val pollingIntervals: Observable<Long>,
    private val pollingUnit: TimeUnit,
    private val maxErrorLimit: Int,
    private val scheduler: Scheduler = Schedulers.computation()
) : ObservableTransformer<T, T> {
    override fun apply(upstream: Observable<T>): ObservableSource<T> = upstream.materialize()
        .repeatWhen { completions ->
            Observables
                .zip(completions, pollingIntervals) { _, pollingInterval -> pollingInterval }
                .flatMap { pollingInterval -> Observable.timer(pollingInterval, pollingUnit, scheduler) }
        }
        // Keep a running count of the number of errors encountered so far.
        .map { item -> (if (item.isOnError) 1 else 0) to item }
        .scan { (errorCount, _), (isOnError, item) -> errorCount + isOnError to item }
            //For demo purposes VVV
        .observeOn(AndroidSchedulers.mainThread())
        .doOnNext {(errorCount, item) ->
            if (item.isOnError) {
                Toast.makeText(App.getContext(), "ErrorCount: $errorCount", Toast.LENGTH_SHORT)
                    .show()
            }
            errorCount to item
        }
        .observeOn(scheduler)
            //End For demo purposes ^^^
        .filter { (errorCount, item) ->
            // If we have reached our error limit, pass all notifications through. onError and onComplete will
            // terminate the stream when dematerialized.
            // Otherwise, only pass through onNext and onComplete
            errorCount >= maxErrorLimit || item.isOnNext || item.isOnComplete
        }
        .map { (_, item) -> item }
        .dematerialize()

}
