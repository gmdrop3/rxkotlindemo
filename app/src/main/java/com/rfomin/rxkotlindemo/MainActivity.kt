package com.rfomin.rxkotlindemo

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.rfomin.rxkotlindemo.fragments.MovieListFragment
import com.rfomin.rxkotlindemo.viewmodel.MovieViewModel

class MainActivity : FragmentActivity(), MovieListFragment.MovieListInteractionListener {
    private var movieViewModel: MovieViewModel? = null
    private var mNavController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (findViewById<View>(R.id.navHost) != null) {
            mNavController = Navigation.findNavController(this, R.id.navHost)
        }

        movieViewModel = ViewModelProviders.of(this).get(MovieViewModel::class.java)

    }

    override fun onItemClick() {
        mNavController?.navigate(R.id.movieDetailsFragment)
    }

    override fun onBackPressed() {
        when {
            mNavController == null -> super.onBackPressed()
            mNavController?.currentDestination?.id == R.id.movieListFragment -> super.onBackPressed()
            else -> mNavController?.popBackStack()
        }
    }
}
