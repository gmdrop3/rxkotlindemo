package com.rfomin.rxkotlindemo.dataprovider

import androidx.lifecycle.LiveData
import com.rfomin.rxkotlindemo.database.model.MovieModel
import io.reactivex.Observable

interface DataProvider {
    fun getMoviesObservable(): Observable<List<MovieModel>>
    fun getMoviesLiveData(): LiveData<List<MovieModel>>

    fun searchMovie(searchString: String)
    fun getAllMovies()

    fun demoSearchMovieWithErrors(searchString: String)

    fun onDestroy()
}