package com.rfomin.rxkotlindemo.dataprovider

import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import com.rfomin.rxkotlindemo.App
import com.rfomin.rxkotlindemo.database.model.MovieModel
import com.rfomin.rxkotlindemo.mapper.toMovieModelList
import com.rfomin.rxkotlindemo.rx.RxRepeat
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

class MovieDataProvider: DataProvider{
    private val api = App.getDaggerAppComponent().provideApi()
    private val database = App.getDaggerAppComponent().provideDataBase()
    private var searchDisposable: Disposable? = null
    private var errorDisposable: Disposable? = null
    private var compositeDisposable = CompositeDisposable()

    private val filteredMoviesObservable: BehaviorSubject<List<MovieModel>> = BehaviorSubject.create()

    override fun getMoviesLiveData(): LiveData<List<MovieModel>> {
        searchMovie("")
        return database.getMovieDao().getAllMoviesLiveData()
    }

    override fun getMoviesObservable(): Observable<List<MovieModel>> {
        return filteredMoviesObservable
    }

    override fun getAllMovies() {
        searchMovie("")
    }

    override fun searchMovie(searchString: String) {
        searchDisposable?.let {
            it.dispose()
            compositeDisposable.remove(it)
        }

        searchDisposable = database.getMovieDao().searchMoviesRx("%$searchString%")
            .subscribe (
                { movieModelList ->
                    filteredMoviesObservable.onNext(movieModelList)
                },
                {
                    filteredMoviesObservable.onNext(listOf())
                }
            )

        searchDisposable?.let { compositeDisposable.add(it) }

        getFilteredMoviesFromTMDB(searchString)
    }

    override fun demoSearchMovieWithErrors(searchString: String) {
        getFilteredMoviesFromTMDBWithError(searchString)
    }

    private fun checkMainThread(message: String) {
        val isMainThread = (Looper.myLooper() == Looper.getMainLooper()).toString()
        println("IsMain? = $isMainThread >>> $message")
    }

    private fun getFilteredMoviesFromTMDB(searchString: String = "") {
        Observable.just(searchString)
            .flatMapSingle {
                when {
                    it.isEmpty() -> api.getPopularMovies()
                    else -> api.getSearchMovie(searchString)
                }
            }
            .subscribeOn(Schedulers.io())
            .map { tmdbMovieResponse ->
                tmdbMovieResponse.toMovieModelList()
            }
            .subscribe (
                { movieModelList ->
                    database.getMovieDao().insert(movieModelList)
                },
                { throwable ->
                    Log.e("MovieDataProvider","error ${throwable.message}")
                }
            )
            .addTo(compositeDisposable)
    }

    private fun getFilteredMoviesFromTMDBWithError(searchString: String = "") {
        var retries = 0
        errorDisposable?.apply { dispose() }
        errorDisposable = Observable.just(searchString)
            .flatMapSingle {
                ++retries
                val single =
                    when {
                        retries <= 3 -> Single.error(UnknownHostException("artificial error for demo"))
                        it.isEmpty() -> api.getPopularMovies()
                        else -> api.getSearchMovie(searchString)
                    }
                single
            }
            .compose(
                RxRepeat(
                    //pollingIntervals = Observable.just(1L, 5L, 5L, 5L, 10L, 10L),
                    pollingIntervals = Observable.just(1L, 1L, 1L, 1L, 1L, 1L),
                    pollingUnit = TimeUnit.SECONDS,
                    maxErrorLimit = 6
                )
            )
            .subscribeOn(Schedulers.io())
            .map { tmdbMovieResponse ->
                tmdbMovieResponse.toMovieModelList()
            }
            .subscribe (
                { movieModelList ->
                    filteredMoviesObservable.onNext(movieModelList)
                },
                { throwable ->
                    Log.e("MovieDataProvider","error ${throwable.message}")
                }
            )
        errorDisposable?.let { compositeDisposable.add(it) }

        someFunction()



    }

    private fun someFunction() {
        Observable.interval(2, TimeUnit.SECONDS)
            .subscribe { println("observer 1: $it") }
            .addTo(compositeDisposable)

        Observable.interval(3, TimeUnit.SECONDS)
            .subscribe { println("observer 2: $it") }
            .addTo(compositeDisposable)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}
