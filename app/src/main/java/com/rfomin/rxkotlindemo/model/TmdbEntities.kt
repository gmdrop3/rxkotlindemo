package com.rfomin.rxkotlindemo.model

data class TmdbMovie(
    val id: Int,
    val vote_average: Double,
    val title: String,
    val overview: String,
    val adult: Boolean,
    val poster_path: String
)

data class TmdbMovieResponse(
    val page: Int,
    val results: List<TmdbMovie>,
    val total_results: Int,
    val total_pages: Int
)

