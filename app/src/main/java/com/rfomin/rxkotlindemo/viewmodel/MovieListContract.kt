package com.rfomin.rxkotlindemo.viewmodel

import com.rfomin.rxkotlindemo.database.model.Movie

interface MovieListContract {
    fun onListItemClick(itemData: Movie)
}