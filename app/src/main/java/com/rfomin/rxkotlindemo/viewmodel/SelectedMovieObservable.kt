package com.rfomin.rxkotlindemo.viewmodel

import com.rfomin.rxkotlindemo.database.model.Movie
import io.reactivex.Observable

interface SelectedMovieObservable {
    fun getSelectedMovieObservable(): Observable<Movie>
}