package com.rfomin.rxkotlindemo.viewmodel

import androidx.lifecycle.LiveData
import com.rfomin.rxkotlindemo.database.model.MovieModel

interface AllMoviesLiveData {
    fun getAllMoviesLiveData(): LiveData<List<MovieModel>>
}