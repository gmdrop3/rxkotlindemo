package com.rfomin.rxkotlindemo.viewmodel

interface MovieRequester {
    fun searchMovie(searchString: String)
    fun requestAllMovies()
}