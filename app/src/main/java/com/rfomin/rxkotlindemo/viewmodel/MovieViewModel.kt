package com.rfomin.rxkotlindemo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rfomin.rxkotlindemo.App
import com.rfomin.rxkotlindemo.database.model.EmptyMovie
import com.rfomin.rxkotlindemo.database.model.Movie
import com.rfomin.rxkotlindemo.database.model.MovieModel
import com.rfomin.rxkotlindemo.dataprovider.DataProvider
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

class MovieViewModel : ViewModel(),
    AllMoviesObservable,
    AllMoviesLiveData,
    MovieRequester,
    MovieListContract,
    SelectedMovieObservable {
    private val dataProvider: DataProvider = App.getDaggerAppComponent().provideDataProvider()

    private val mutableMovieModelListLiveData: MutableLiveData<List<MovieModel>> = MutableLiveData()

    override fun getAllMoviesObservable(): Observable<List<MovieModel>> {
        return dataProvider.getMoviesObservable()
            .doOnNext { movieModelList ->
                updateSelection(movieModelList)
                mutableMovieModelListLiveData.postValue(movieModelList)
            }
    }

    override fun getAllMoviesLiveData(): LiveData<List<MovieModel>> {
        return mutableMovieModelListLiveData
    }

    override fun searchMovie(searchString: String) {
        dataProvider.searchMovie(searchString)
    }

    override fun requestAllMovies() {
        dataProvider.getAllMovies()
    }

    private val selectedMovieSubject: BehaviorSubject<Movie> = BehaviorSubject.create()
    override fun getSelectedMovieObservable(): Observable<Movie> = selectedMovieSubject

    override fun onListItemClick(itemData: Movie) {
        selectedMovieSubject.onNext(itemData)
    }

    private fun updateSelection(movies: List<MovieModel>) {
        val selectedMovieId = (selectedMovieSubject.value as? MovieModel)?.id ?: 0
        val movie = movies.find { it.id == selectedMovieId }
        if (movie == null) {
            selectedMovieSubject.onNext(EmptyMovie.model())
        }
    }

    override fun onCleared() {
        dataProvider.onDestroy()
        super.onCleared()
    }
}