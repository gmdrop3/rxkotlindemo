package com.rfomin.rxkotlindemo.viewmodel

import com.rfomin.rxkotlindemo.database.model.MovieModel
import io.reactivex.Observable

interface AllMoviesObservable {
    fun getAllMoviesObservable(): Observable<List<MovieModel>>
}