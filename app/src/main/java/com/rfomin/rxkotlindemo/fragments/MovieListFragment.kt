package com.rfomin.rxkotlindemo.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.jakewharton.rxbinding2.widget.textChanges
import com.rfomin.rxkotlindemo.App
import com.rfomin.rxkotlindemo.R
import com.rfomin.rxkotlindemo.adapters.MovieListAdapter
import com.rfomin.rxkotlindemo.database.model.MovieModel
import com.rfomin.rxkotlindemo.mapper.toMovieModelList
import com.rfomin.rxkotlindemo.viewmodel.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_movie_list.*
import java.util.concurrent.TimeUnit

class MovieListFragment : Fragment() {

    private var listener: MovieListInteractionListener? = null
    private var viewModel: AllMoviesObservable? = null

    private var compositeDisposable = CompositeDisposable()
    private val listClickListener: MovieListAdapter.ListItemClickListener
    private var readyToSearch = false

    init {
        listClickListener = object: MovieListAdapter.ListItemClickListener {
            override fun onItemClicked(item: MovieModel) {
                (viewModel as? MovieListContract)?.onListItemClick(item)
                listener?.onItemClick()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movie_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        search_movie.addTextChangedListener( object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val text: String = charSequence?.toString() ?: ""
                (viewModel as? MovieRequester)?.searchMovie(text)
            }
        })

        search_movie_alternative
            .textChanges()
            .filter { readyToSearch }
            .debounce(1, TimeUnit.SECONDS)
            .map {
                it.toString()
            }
            .filter {
                it.length >= 3 || it.isEmpty()
            }
            .flatMapSingle { text ->
                val single = if (text.isEmpty()) {
                    App.getDaggerAppComponent().provideApi().getPopularMovies()
                } else {
                    App.getDaggerAppComponent().provideApi().getSearchMovie(text)
                }
                single
            }
            .map { tmdbMovieResponse ->
                tmdbMovieResponse.toMovieModelList()
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { movieModelList ->
                    setMovies(movieModelList)
                }, { throwable ->
                    Log.e("MovieListFragment","error ${throwable.message}")
                }
            )
            .addTo(compositeDisposable)

        val dataProvider = App.getDaggerAppComponent().provideDataProvider()
        search_movie_repetitive
            .textChanges()
            .filter { readyToSearch }
            .debounce(1, TimeUnit.SECONDS)
            .map {
                it.toString()
            }
            .filter {
                it.length >= 3 || it.isEmpty()
            }
            .subscribe { text ->
                dataProvider.demoSearchMovieWithErrors(text)
            }
            .addTo(compositeDisposable)
    }

    private fun setMovies(movies: List<MovieModel>) {
        if (movies.isNotEmpty()) {
            recyclerview_list.adapter = MovieListAdapter(movies, listClickListener)
            recyclerview_list.visibility = View.VISIBLE
            no_items.visibility = View.GONE
        } else {
            recyclerview_list.visibility = View.GONE
            no_items.visibility = View.VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()
        readyToSearch = true
    }

    override fun onPause() {
        readyToSearch = false
        super.onPause()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val fragmentActivity = activity
        if (fragmentActivity != null) {
            viewModel = ViewModelProviders.of(fragmentActivity).get(MovieViewModel::class.java)
            (viewModel as? MovieRequester)?.requestAllMovies()

            if (activity != null) {
                val listDisposable = viewModel?.let { viewModel ->
                    viewModel.getAllMoviesObservable()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { movieModelList ->
                            setMovies(movieModelList)
                        }
                }
                if (listDisposable != null) {
                    compositeDisposable.add(listDisposable)
                }

                (viewModel as? AllMoviesLiveData)?.getAllMoviesLiveData()?.observe(viewLifecycleOwner,
                    Observer {movieModelList ->
                        //setMovies(movieModelList)
                    })
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MovieListInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }

    interface MovieListInteractionListener {
        fun onItemClick()
    }
}
