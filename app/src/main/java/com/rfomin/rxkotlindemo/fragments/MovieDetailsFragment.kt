package com.rfomin.rxkotlindemo.fragments

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

import com.rfomin.rxkotlindemo.R
import com.rfomin.rxkotlindemo.api.ApiFactory.Companion.IMAGE_URL
import com.rfomin.rxkotlindemo.database.model.EmptyMovie
import com.rfomin.rxkotlindemo.database.model.MovieModel
import com.rfomin.rxkotlindemo.viewmodel.MovieViewModel
import com.rfomin.rxkotlindemo.viewmodel.SelectedMovieObservable
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.movie_details_fragment.*
import java.lang.Exception

class MovieDetailsFragment : Fragment() {
    private var compositeDisposable = CompositeDisposable()
    private var selectedMovieObservable: SelectedMovieObservable? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.movie_details_fragment, container , false)
    }

    private fun setMovieDetails(movieDetails: MovieModel) {
        scroll_view.visibility = View.VISIBLE
        movie_name.text = movieDetails.title
        movie_description.text = movieDetails.overview
        movie_rating.text = movieDetails.vote_average.toString()
        scroll_view.scrollTo(0, 0)
        progressBar.visibility = View.VISIBLE
        Picasso.get()
            .load("$IMAGE_URL${movieDetails.poster_path}")
            .centerInside()
            .fit()
            .into(image, object : Callback {
                override fun onSuccess() {
                    progressBar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    progressBar.visibility = View.GONE
                    image.setImageResource(R.drawable.no_image_available)
                }
            })
    }

    private fun hideDetails() {
        scroll_view.visibility = View.GONE
        activity?.let {
            if (it.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                it.onBackPressed()
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val fragmentActivity = activity
        if (fragmentActivity != null) {
            selectedMovieObservable = ViewModelProviders.of(fragmentActivity).get(MovieViewModel::class.java)

            val disposable = selectedMovieObservable?.let { selectedMovieObservable ->
                selectedMovieObservable.getSelectedMovieObservable()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {movie ->
                        when (movie) {
                            is EmptyMovie -> hideDetails()
                            is MovieModel -> setMovieDetails(movie)
                        }

                    }
            }
            if (disposable != null) {
                compositeDisposable.add(disposable)
            }
        }
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}
